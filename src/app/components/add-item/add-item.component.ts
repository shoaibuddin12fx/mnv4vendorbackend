import { Component, OnInit, ViewChild, ElementRef, Input } from "@angular/core";
import { DataApiService } from "../../services/data-api.service";
import { BookInterface } from "../../models/book";
import { NgForm } from "@angular/forms";

import { FirebaseService } from "../../services/firebase.service";
import { Router } from "@angular/router";

import { CategoryInterface } from "../../models/category";
import { AuthService } from "src/app/services/auth.service";
import { UserInterface } from "src/app/models/user";

@Component({
  selector: "app-add-item",
  templateUrl: "./add-item.component.html",
  styleUrls: ["./add-item.component.css"],
})
export class AddItemComponent implements OnInit {
  available: any;
  category: any;
  description: any;
  image: any;
  name: any;
  price: any;
  stock: any;
  categories: any;
  percent: any;
  sub_category: any;
  sort: any;
  status: any;
  saleprice: any;
  new_arrival: any;
  feature: any;
  ven_key: any;
  category_details: any;

  uid: any;
  categoryList: any;
  sub_categoryList: any;
  userDetails: any;

  private CategoryInterface: CategoryInterface[];
  public isAdmin: any = null;

  constructor(
    private dataApi: DataApiService,
    private firebaseService: FirebaseService,
    private router: Router,
    private authService: AuthService
  ) {}
  @ViewChild("btnClose") btnClose: ElementRef;
  @Input() userUid: string;

  onSaveBook(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      // New
      bookForm.value.userUid = this.userUid;
      this.dataApi.addBook(bookForm.value);
    } else {
      // Update
      this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    this.btnClose.nativeElement.click();
  }

  ngOnInit() {
    this.firebaseService
      .getCategories()
      .snapshotChanges()
      .subscribe((categories) => {
        // Using snapshotChanges() method to retrieve list of data along with metadata($key)
        this.categoryList = [];
        this.sub_categoryList = [];

        categories.forEach((item) => {
          console.log(item);

          const a = item.payload.toJSON();
          a["$key"] = item.key;

          console.log(a);

          this.categoryList.push(a as CategoryInterface);
        });
      });
  }

  onItemAddSubmit() {
    this.authService.isAuth().subscribe((auth) => {
      if (auth) {
        this.uid = auth.uid;
        console.log("User Id Is " + this.uid);
        this.firebaseService
          .getUserDetail(this.uid)
          .snapshotChanges()
          .subscribe((user) => {
            this.userDetails = [];

            const b = user.payload.toJSON();

            this.userDetails.push(b as UserInterface);
            console.log(this.userDetails[0].ven_key);
            this.ven_key = this.userDetails[0].ven_key;
            this.firebaseService
              .getCategoryDetails(this.categories)
              .snapshotChanges()
              .subscribe((category) => {
                this.category_details = [];

                const res = category.payload.toJSON();

                if (category.key != null || category.key != "null") {
                  res["$key"] = category.key;
                }

                console.log(category);

                this.category_details = res as CategoryInterface;

                console.log(this.category_details);

                const item = {
                  available: this.available,
                  description: this.description,
                  name: this.name,
                  price: this.price,
                  stock: this.stock,
                  categories: this.categories,
                  percent: this.percent,
                  restaurant_image: this.category_details.restaurant_image,
                  restaurant_name: this.category_details.restaurant_name,
                  restaurant_lat: this.category_details.restaurant_lat,
                  restaurant_long: this.category_details.restaurant_long,
                  res_id: this.category_details.res_id,
                  user_id: this.category_details.user_id,
                  saleprice: this.saleprice,
                  sub_category: this.sub_category,
                  vendor: this.ven_key,
                  image: this.image,
                  feature: this.feature,
                  new_arrival: this.new_arrival,
                  sort: this.sort,
                  status: this.status,
                };

                this.firebaseService.addItem(item);
                this.router.navigate(['items']);
              });
          });
      }
    });
  }

  setParentCategory(cat_id) {
    console.log(cat_id);
    this.sub_categoryList = this.categoryList.filter(
      (x) => x.parent_category_id == cat_id
    );
  }
}
