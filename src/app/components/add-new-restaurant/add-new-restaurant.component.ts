import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';

import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { UserInterface } from 'src/app/models/user';

@Component({
  selector: 'app-add-new-restaurant',
  templateUrl: './add-new-restaurant.component.html',
  styleUrls: ['./add-new-restaurant.component.css'],
})
export class AddNewRestaurantComponent implements OnInit {
  address?: string;
  description?: string;
  image?: string;
  info?: string;
  lat?: string;
  long?: string;
  mark?: string;
  option?: string;
  outlet?: string;
  phonenumber?: string;
  title?: string;
  ven_name?: string;

  userDetails?: any;
  uid?: any;
  constructor(
    private dataApi: DataApiService,
    private firebaseService: FirebaseService,
    private router: Router,
    private authService: AuthService
  ) {}
  @ViewChild('btnClose') btnClose: ElementRef;
  @Input() userUid: string;

  onSaveBook(bookForm: NgForm): void {
    if (bookForm.value.id == null) {
      // New
      bookForm.value.userUid = this.userUid;
      this.dataApi.addBook(bookForm.value);
    } else {
      // Update
      this.dataApi.updateBook(bookForm.value);
    }
    bookForm.resetForm();
    this.btnClose.nativeElement.click();
  }

  ngOnInit() {}
  onAddRestaurant() {
    this.authService.isAuth().subscribe((auth) => {
      if (auth) {
        this.uid = auth.uid;
        console.log('User Id Is ' + this.uid);
        this.firebaseService
          .getUserDetail(this.uid)
          .snapshotChanges()
          .subscribe((user) => {
            this.userDetails = [];

            const b = user.payload.toJSON();

            this.userDetails.push(b as UserInterface);
            this.ven_name = this.userDetails[0].ven_key;

            const restaurant = {
              address: this.address,
              description: this.description,
              image: this.image,
              info: this.info,
              lat: this.lat,
              long: this.long,
              mark: this.mark,
              option: this.option,
              outlet: this.outlet,
              phonenumber: this.phonenumber,
              title: this.title,
              ven_name: this.ven_name,
            };

            // The ven_key Of Current User
            console.log(this.ven_name);
            // Inserting Data Into FireBase
            this.firebaseService.addRestaurant(restaurant);
            this.router.navigate(['restaurants']);
          });
      }
    });
  }
}
