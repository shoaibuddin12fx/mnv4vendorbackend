import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant';
import { CategoryInterface } from '../../models/category';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
})
export class CategoriesComponent implements OnInit {
  restaurants: any;
  restaurant2: any;

  private books: BookInterface[];

  private RestaurantInterface: RestaurantInterface[];
  private CategoryInterface: CategoryInterface[];
  public isAdmin: any = null;
  public userUid: string = null;

  categories: any;
  uid: any;
  userDetails: any;
  ven_key: any;
  res_id: any;
  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.isAuth().subscribe((auth) => {
      if (auth) {
        this.uid = auth.uid;
        console.log('User Id Is ' + this.uid);
        this.firebaseService
          .getUserDetail(this.uid)
          .snapshotChanges()
          .subscribe((user) => {
            this.userDetails = [];

            const b = user.payload.toJSON();

            this.userDetails.push(b as UserInterface);
            this.ven_key = this.userDetails[0].ven_key;
            this.firebaseService
              .getRestaurantUser(this.ven_key)
              .snapshotChanges()
              .subscribe((restaurants) => {
                this.restaurants = [];

                restaurants.forEach((item) => {
                  console.log(item);

                  const c = item.payload.toJSON();
                  c['$key'] = item.key;

                  this.restaurants.push(c as RestaurantInterface);
                  this.restaurants.forEach((element) => {
                    this.res_id = element.$key;
                  });

                  this.firebaseService
                    .getCategoryUser(this.res_id)
                    .snapshotChanges()
                    .subscribe((categories) => {
                      this.categories = [];
                      categories.forEach((category) => {
                        const a = category.payload.toJSON();
                        a['$key'] = category.key;

                        this.categories.push(a as CategoryInterface);
                      });
                    });
                });
              });
          });
      }
    });
  }


  onRestaurantDelete(id) {
    this.firebaseService.deleteRestaurant(id);
    this.router.navigate(['/restaurants']);
  }

  goToRestaurantDetails(restaurant) {
    console.log(restaurant);
    this.router.navigate(['/restaurants']);
  }
}
