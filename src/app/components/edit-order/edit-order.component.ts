import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {Router} from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant'; 
import { CategoryInterface } from '../../models/category'; 
import { OrderInterface } from '../../models/order'; 

import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {
	
	restaurants : any;
	restaurant2: any;
	
	id: any;
	
	restaurant:any;
	imageUrl:any;
	categories: any;
	
	order_id:any;
	order_details:any;
	status:any;
	checked:any;
	
	private RestaurantInterface: RestaurantInterface[];  
	private CategoryInterface: CategoryInterface[];  
	private OrderInterface: OrderInterface[];  
  
  
	cat_id:any;
	category_details:any;

	user_details: any;
	
	total: any;
	user_id: any;
	order_status_check: any;
	
	order_details_status: any;
	
	public taxPercent: any;
	public fee: any;
	public feePrice: any;

  constructor(private firebaseService:FirebaseService, private authService: AuthService , 
  private router: Router, private route: ActivatedRoute,private afsAuth: AngularFireAuth) { }

  ngOnInit() {
	  
	  	this.id = this.route.snapshot.params['id'];
			
			console.log(this.id);
			
			this.user_id = this.afsAuth.auth.currentUser.uid;
	  
			console.log(this.user_id);
		
			
			this.firebaseService.getOrderDetail(this.id).on('value', (snapshot) => {
							
							this.order_details = snapshot.val();
		  
							console.log(this.order_details);
		  
							 this.total = 0;
							  this.order_details.items.forEach(items => {
									console.log(items);
									
									
									
									if(items.owner_id == this.user_id){
										
										var subTotal = items.quantity * items.price;
										
										console.log(subTotal);
										
										this.total += subTotal;
										
										console.log(this.total);
										
										
									}
							});
		  
		});
		
		
		
		this.firebaseService.getRestaurantCompletedOrderStatus2(this.id).on('value', (snapshot) => {
							
							this.order_details_status = snapshot.val();
		  
							console.log(this.order_details_status);
		  
							 //console.log(order_status_check);
							 
															if(this.order_details_status == null || this.order_details_status == "null"){
																
																this.order_status_check = "Queued";
									
																console.log(this.order_status_check);
																
																
															}
															else{
															
															//	this.order_status_check = res.status;
																
																
																
																this.order_status_check = this.order_details_status.status;
									
																console.log(this.order_status_check);
																
															}
		
		
							/**
								if(this.order_details_status.status != null){
									
									
									this.order_status_check = this.order_details_status.status;
									
									console.log(this.order_status_check);
								}
								else{
								
									
									this.order_status_check = "Queued";
									
									console.log(this.order_status_check);
								}
								
								**/
		  
		});
		
		/**
		this.firebaseService.getRestaurantCompletedOrderStatus(this.id).snapshotChanges().subscribe(order_status_check => {
															
															console.log(order_status_check);
															
															 let res = order_status_check.payload.toJSON(); 
															
															
															console.log(res);
										
															
															
															if(res == null || res == "null"){
																
																
																this.order_status_check = "Queued";
																
																console.log(this.order_status_check);
																
															
																
																
															}
															else{
																
																this.order_status_check = order_status_check.payload.toJSON().status;
																
																console.log(this.order_status_check);
																
																console.log("Status Changed Orders");
																
															}
														
															
															
															
															
															
																
			});
			
			**/
			
			
			this.firebaseService.getTaxPercentInfo().on('value', (snapshot) => {
							
							this.fee = snapshot.val();		

							console.log(this.fee);							
				});
	
	
		console.log(this.fee);
		
	  
  }
  
  
   onStatusOrderSubmit(){
	   
	    console.log(this.fee);
	
	
		this.feePrice = (this.total * this.fee)/100;
	
		console.log(this.feePrice);
	
	
		this.taxPercent = parseFloat(this.feePrice).toFixed(2);
	
		console.log(this.taxPercent);
		
		console.log(this.status);
		console.log(this.total);
		console.log(this.taxPercent);
	  
	  
	  let order_details2= {
		  status : this.status,
		  total : this.total,
		  fee: this.taxPercent,
	  }
	  
	  let order_details3= {
		  status : this.status,
		  fee: this.taxPercent,
		  
	  }
	  
	  let order_details4= {
		  test : "test",
		  
	  }
	  
	  
	  
	  
	  this.firebaseService.createOwnerOrderStatus(this.id,this.order_details);
	  
	  this.firebaseService.updateOrderStatus(this.id,order_details3);
	  //this.firebaseService.updateRestaurantOrderStatus(this.id,order_details);
	  this.firebaseService.updateOwnerOrderStatus(this.id,order_details2);
	  
	  
	  this.firebaseService.updateYearReport(this.order_details,this.total,order_details2,this.taxPercent);
	  
	  this.firebaseService.updateMonthReport(this.order_details,this.total,order_details2,this.taxPercent);
	  
	  this.firebaseService.updateDayReport(this.order_details,this.total,order_details2,this.taxPercent);
	  
	//  this.firebaseService.removeCategorizedOrders(this.id);
	  
	  //this.firebaseService.removeCategorizedOrders(this.id);
	  
	  
	  this.router.navigate(['/orders']);
	  
  }

}
