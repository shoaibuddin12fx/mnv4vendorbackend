import { Component, OnInit, NgZone } from "@angular/core";
import { DataApiService } from "../../services/data-api.service";
import { BookInterface } from "../../models/book";
import { NgForm } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { UserInterface } from "../../models/user";
import { ViewChild, ElementRef, Input } from "@angular/core";
import { FirebaseService } from "../../services/firebase.service";
import { Router } from "@angular/router";

import { RestaurantInterface } from "../../models/restaurant";
import { CategoryInterface } from "../../models/category";
import { OrderInterface } from "../../models/order";
import { ItemInterface } from "src/app/models/item";

import { ActivatedRoute, Params } from "@angular/router";
import { CityInterface } from "src/app/models/city";
import { DistrictInterface } from "src/app/models/district";

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css'],
})
export class OrderDetailsComponent implements OnInit {
  restaurants: any;
  restaurant2: any;

  id: any;

  restaurant: any;
  imageUrl: any;
  categories: any;
  orders: any;

  private RestaurantInterface: RestaurantInterface[];
  private CategoryInterface: CategoryInterface[];
  private OrderInterface: OrderInterface[];

  cat_id: any;
  category_details: any;

  order_id: any;
  order_details: any;
  user_details: any;
  itemList: any;
  vendorList: any;
  userDetails: any;
  uid: any;
  ven_key: any;
  item_img: any;
  cities: any;
  districts: any;

  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private zone: NgZone
  ) {
    // this.id = this.route.snapshot.params['id'];
    // console.log(this.id);
    /**
		this.firebaseService.getNewOrderDetails(this.id).snapshotChanges().subscribe(order => {
					this.order_details = [];



						 let res = order.payload.toJSON();

						 if(order.key != null || order.key != 'null'){
						 res['$key'] = order.key;
						 }

						console.log(order);

						this.order_details = res as OrderInterface;


						console.log(this.order_details);




		});
		**/
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];

    console.log(this.id);

    this.firebaseService.getOrderDetail(this.id).on('value', (snapshot) => {
      this.order_details = snapshot.val();
      this.order_details.items.forEach(async element => {
        this.itemList = [];
        console.log(element);
        this.ven_key = await this.getUserKey();
        console.log(this.ven_key);
        if (element.vendor === this.ven_key) {
          this.itemList.push(element);
        }
      });
      console.log(this.itemList);
      console.log(this.order_details);
    });
    
    this.firebaseService
      .getCities()
      .snapshotChanges()
      .subscribe((cities) => {
        this.cities = [];
        cities.forEach((item) => {

          let b = item.payload.toJSON();
          b["$key"] = item.key;

          this.cities.push(b as CityInterface);
        });
      });

    this.firebaseService
      .getDistricts()
      .snapshotChanges()
      .subscribe((districts) => {
        this.districts = [];
        districts.forEach((item) => {

          let b = item.payload.toJSON();
          b["$key"] = item.key;

          this.districts.push(b as DistrictInterface);
        });
      });

    this.firebaseService
      .getCategories()
      .snapshotChanges()
      .subscribe((categories) => {
        this.categories = [];
        categories.forEach((item) => {

          let b = item.payload.toJSON();
          b["$key"] = item.key;

          this.categories.push(b as CategoryInterface);
        });
      });
  }

  async getUserKey() {
    return new Promise(async resolve => {
      this.authService.isAuth().subscribe((auth) => {
        if (auth) {
          this.uid = auth.uid;
          console.log('User Id Is ' + this.uid);
          this.firebaseService
            .getUserDetail(this.uid)
            .snapshotChanges()
            .subscribe((user) => {
              this.userDetails = [];
  
              const b = user.payload.toJSON();
              this.userDetails.push(b as UserInterface);
              console.log(this.userDetails[0].ven_key);
              console.log(this.userDetails);
              resolve(this.userDetails[0].ven_key);
            });
        }
      });
    })
  }
  getCityName($key) {
    return this.cities.find((x) => x.$key == $key).name;
  }

  getDistrictName($key) {
    return this.districts.find((x) => x.$key == $key).name;
  }

  getCategoryName($key) {
    if ($key) {
      return this.categories.find((x) => x.$key == $key).cat_name;
    } else {
      return;
    }
  }
}
