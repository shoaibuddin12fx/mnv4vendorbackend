import { Component, OnInit, NgZone } from "@angular/core";
import { DataApiService } from "../../services/data-api.service";
import { BookInterface } from "../../models/book";
import { NgForm } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { UserInterface } from "../../models/user";
import { ViewChild, ElementRef, Input } from "@angular/core";
import { FirebaseService } from "../../services/firebase.service";
import { Router } from "@angular/router";

import { RestaurantInterface } from "../../models/restaurant";

import { OrderInterface } from "../../models/order";

@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html",
  styleUrls: ["./orders.component.css"],
})
export class OrdersComponent implements OnInit {
  orders: any;
  orderList: any;
  order_status_check: any;
  isLoading = true;
  restaurant: any;
  uid: any;
  ven_key: any;
  userDetails: any;
  vendors:any;
  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router,
    private zone: NgZone
  ) {}

  ngOnInit() {
    this.authService.isAuth().subscribe((auth) => {
      if (auth) {
        this.uid = auth.uid;
        console.log("User Id Is " + this.uid);
        this.firebaseService
          .getUserDetail(this.uid)
          .snapshotChanges()
          .subscribe((user) => {
            this.userDetails = [];

            const b = user.payload.toJSON();

            this.userDetails.push(b as UserInterface);
            console.log(this.userDetails[0].ven_key);
            this.ven_key = this.userDetails[0].ven_key;
            this.firebaseService

              .getRestaurantOrders(this.ven_key)
              .snapshotChanges()
              .subscribe((orders) => {
                // this.zone.run(() => {

                // Using snapshotChanges() method to retrieve list of data along with metadata($key)
                this.orderList = [];

                orders.forEach((item) => {
                  console.log(item);

                  let a = item.payload.toJSON();
                  a["$key"] = item.key;

                  this.zone.run(() => {
                    Object.keys(a['vendors']).forEach((key) => {
                      this.vendors = (a['vendors'][key]);
                      console.log(this.vendors);
                    });
                    if (this.vendors === this.ven_key) {
                      this.orderList.push(a as OrderInterface);
                    }
                  });
                });

                console.log(this.orderList);

                //    this.orderList.forEach((item) => {
                //      console.log(item);

                //      this.firebaseService
                //        .getRestaurantCompletedOrderStatus(item.$key)
                //        .snapshotChanges()
                //        .subscribe((order_status_check) => {
                //          console.log(order_status_check);

                //          let res = order_status_check.payload.toJSON();

                //          console.log(res);

                //          //this.restaurant = res as OrderInterface;

                //          //console.log(this.restaurant);

                //          if (res == null || res == "null") {
                //            this.order_status_check = "Queued";

                //            console.log(this.order_status_check);

                //            this.orders.push(item as OrderInterface);
                //          } else {
                //            //	this.order_status_check = res.status;

                //            console.log("Status Changed Orders");
                //          }
                // if (res.status) {
                //   this.order_status_check = this.restaurant.statu;

                //   console.log(this.order_status_check);
                // } else {
                //   this.order_status_check = "Queued";

                //   console.log(this.order_status_check);

                //   this.orders.push(res as OrderInterface);
                // }
              });
          });
        // });
      }
    });
  }
}
