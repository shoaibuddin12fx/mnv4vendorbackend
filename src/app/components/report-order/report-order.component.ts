import { Component, OnInit , NgZone } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {Router} from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant'; 
import { CategoryInterface } from '../../models/category'; 
import { OrderInterface } from '../../models/order'; 

import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-report-order',
  templateUrl: './report-order.component.html',
  styleUrls: ['./report-order.component.css']
})
export class ReportOrderComponent implements OnInit {
	
	restaurants : any;
	restaurant2: any;
	
	id: any;
	
	restaurant:any;
	imageUrl:any;
	categories: any;
	
  
	private RestaurantInterface: RestaurantInterface[];  
	private CategoryInterface: CategoryInterface[];  
	private OrderInterface: OrderInterface[];  
  
  
	cat_id:any;
	category_details:any;
  
	order_id:any;
	order_details:any;
	user_details: any;
	
	total: any;
	
	user_id: any;

  constructor(private firebaseService:FirebaseService, private authService: AuthService , 
  private router: Router, private route: ActivatedRoute, private afsAuth: AngularFireAuth, private zone:NgZone) { 
  
		this.id = this.route.snapshot.params['id'];
			
		console.log(this.id);
		
		this.total = 0;
		
		
	
		this.user_id = this.afsAuth.auth.currentUser.uid;
	  
		console.log(this.user_id);
		
			
				   this.firebaseService.getOrderDetail(this.id).on('value', (snapshot) => {
					   
										this.zone.run(() => {
													
													
														this.order_details = snapshot.val();
														  
														  console.log(this.order_details);
														  
																		this.order_details.items.forEach(items => {
																			console.log(items);
																			
																			
																			
																			if(items.owner_id == this.user_id){
																				
																				var subTotal = items.quantity * items.price;
																				
																				console.log(subTotal);
																				
																				this.total += subTotal;
																				
																				console.log(this.total);
																				
																				
																			}
																	});
							});
					   
					  
		  
		});
  }

  ngOnInit() {
  }

}
