import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';

import {FirebaseService} from '../../services/firebase.service';

import { RestaurantInterface } from '../../models/restaurant'; 
import { CategoryInterface } from '../../models/category'; 
import { MonthReportInterface } from '../../models/monthreport'; 



import { ActivatedRoute, Params } from '@angular/router';

import {Router} from '@angular/router';


import { AuthService } from '../../services/auth.service';

import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
	
	
	public stats: any = {
    today: 0,
    yesterday: 0,
    thisMonth: 0,
    lastMonth: 0,
    thisYear: 0,
    lastYear: 0
  };
  
  reportToday: any;
  todayTotal: any;
  reportMonth: any;
  monthTotal: any;
  reportYear: any;
  yearTotal: any;
  
  todayFee: any;
  monthFee: any;
  yearFee: any;
  
  todays: any;
  lastYear: any;
  lastMonth: any;
  yesterday: any;
  thisYear: any;
  thisMonth: any;
  thisDay: any;
  
  private MonthReportInterface: MonthReportInterface[];  

  constructor(private firebaseService:FirebaseService) { }

  ngOnInit() {
	  
	  let today = new Date();
	this.lastYear = today.getFullYear() - 1;
	this.lastMonth = (today.getMonth() > 0) ? today.getMonth() : 12;
	this.yesterday = new Date(Date.now() - 86400000);
	this.thisYear = today.getFullYear();
	this.thisMonth = today.getMonth() + 1;
	this.thisDay = today.getDate();
	  
	  console.log("today " + today);
	  console.log("last Year " + this.lastYear);
	  console.log("lastmonth "  + this.lastMonth);
	  console.log("yesterday " + this.yesterday);
	  console.log("This year " + this.thisYear);
	  console.log("This month " + this.thisMonth);
	  console.log("This Date " + this.thisDay);
	  
	  
	  this.firebaseService.getTodayReport(this.thisYear,this.thisMonth,this.thisDay).snapshotChanges().subscribe(reportToday =>{
		  console.log(reportToday);
		  
		    this.reportToday = [];
			
			let res = reportToday.payload.toJSON(); 
			
			
					res['$key'] = reportToday.key;
			
						
			console.log(reportToday);
			this.reportToday = res as MonthReportInterface;
			
						
			console.log(this.reportToday);
		  
		    //this.reportToday = reportToday;
		  
			this.todayTotal = this.reportToday.total;
			this.todayFee = this.reportToday.fee;
	  });
	  
	  this.firebaseService.getMonthReport(this.thisYear,this.thisMonth).snapshotChanges().subscribe(reportMonth =>{
			
			console.log(reportMonth);
			
			this.reportMonth = [];
		  
			let res = reportMonth.payload.toJSON(); 
			res['$key'] = reportMonth.key;
						
			console.log(reportMonth);
			this.reportMonth = res as MonthReportInterface;
			
						
			console.log(this.reportMonth);
		  //this.reportMonth = reportMonth;
		  
		  this.monthTotal = this.reportMonth.total;
		  this.monthFee = this.reportMonth.fee;
	  });
	  
	  
	  
	  this.firebaseService.getYearReport(this.thisYear).snapshotChanges().subscribe(reportYear =>{
		  console.log(reportYear);
		  
			this.reportYear = [];
		  
			let res = reportYear.payload.toJSON(); 
			res['$key'] = reportYear.key;
						
			console.log(reportYear);
			this.reportYear = res as MonthReportInterface;
			
						
			console.log(this.reportYear);
		  
		  //this.reportYear = reportYear;
		  
			this.yearTotal = this.reportYear.total;
			this.yearFee = this.reportYear.fee;
	  });
	  
	
  }

}
