import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { ViewChild, ElementRef, Input } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';

import { RestaurantInterface } from '../../models/restaurant';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.css'],
})
export class RestaurantsComponent implements OnInit {
  restaurants: any;
  restaurant2: any;

  private books: BookInterface[];

  private RestaurantInterface: RestaurantInterface[];
  public isAdmin: any = null;
  public userUid: string = null;
  uid: any;
  userDetails: any;
  ven_key: any;

  constructor(
    private firebaseService: FirebaseService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit() {
    this.authService.isAuth().subscribe((auth) => {
      if (auth) {
        this.uid = auth.uid;
        console.log('User Id Is ' + this.uid);
        this.firebaseService
          .getUserDetail(this.uid)
          .snapshotChanges()
          .subscribe((user) => {
            this.userDetails = [];

            const b = user.payload.toJSON();

            this.userDetails.push(b as UserInterface);
            console.log(this.userDetails[0].ven_key);
            this.ven_key = this.userDetails[0].ven_key;
            this.firebaseService
              .getRestaurantUser(this.ven_key)
              .snapshotChanges()
              .subscribe((restaurants) => {
                this.restaurants = [];

                restaurants.forEach((item) => {
                  console.log(item);
                  const b = item.payload.toJSON();
                  b['$key'] = item.key;

                  console.log(b);
                  this.restaurants.push(b as RestaurantInterface);
                });
              });
          });
      }
    });
  }

  onRestaurantDelete(id) {
    this.firebaseService.deleteRestaurant(id);
    this.router.navigate(['/restaurants']);
  }

  goToRestaurantDetails(restaurant) {
    console.log(restaurant);
    this.router.navigate(['/restaurants']);
  }
}
