import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { UserInterface } from 'src/app/models/user';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  constructor(
    public afAuth: AngularFireAuth,
    private router: Router,
    private authService: AuthService,
    private firebaseService: FirebaseService
  ) {}
  isError = false;
  public email = "";
  public password = "";
  uid: any;
  userDetail: any;
  vendor: any;
  ngOnInit() {
    this.onLoginCheckUser();
  }

  onLogin(): void {
    this.authService
      .loginEmailUser(this.email, this.password)
      .then((res) => {})
      .catch((err) => console.log("err", err.message));
  }

  onLoginGoogle(): void {
    this.authService
      .loginGoogleUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log("err", err.message));
  }
  onLoginFacebook(): void {
    this.authService
      .loginFacebookUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log("err", err.message));
  }

  onLogout() {
    this.authService.logoutUser();
  }
  onLoginRedirect(): void {
    this.router.navigate(["/restaurants"]);
  }

  onLoginCheckUser() {
    this.authService.isAuth().subscribe((auth) => {
      if (auth) {
        this.uid = auth.uid;
        this.firebaseService
          .getUserDetail(this.uid)
          .snapshotChanges()
          .subscribe((user) => {
            this.userDetail = [];

            const a = user.payload.toJSON();

            this.userDetail.push(a as UserInterface);
            console.log(this.userDetail);
            this.vendor = this.userDetail[0].roles.vendor;
            console.log(this.uid);

            if (this.vendor === true) {
              alert("Welcome To Vendor Page");
              this.onLoginRedirect();
            } else {
              alert("You Don't Have Access To This Page");
              this.onLogout();
            }
          });
      }
    });
  }
}
