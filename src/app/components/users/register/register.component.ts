import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

import * as firebase from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private router: Router,
    private authService: AuthService,
    private storage: AngularFireStorage
  ) {}
  @ViewChild('imageUser') inputImageUser: ElementRef;

  isError = false;
  public email;
  public password;
  public displayName;
  public lastName;
  public address;
  public cardnumber;
  public europeResult;
  public nation;
  public phone;

  uploadPercent: number;
  urlImage: Observable<string>;

  ngOnInit() {}

  onUpload(e) {
    // console.log('subir', e.target.files[0]);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `uploads/profile_${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    task.percentageChanges().subscribe( v => {
      this.uploadPercent = v;
    });
    
    task
      .snapshotChanges()
      .pipe(finalize(() => (this.urlImage = ref.getDownloadURL())))
      .subscribe();
  }
  onAddUser() {
    this.authService
      .registerUser(this.email, this.password)
      .then((res) => {
        this.authService.isAuth().subscribe((user) => {
          if (user) {
            console.log(user);

            firebase.database().ref('/users').child(user.uid).update({
              displayName: this.displayName,
              ownerId: user.uid,
              email: this.email,
              address: this.address,
              phone: this.phone,
              lastName: this.lastName,
              facebook: false,
              cardnumber: this.cardnumber,
              europeResult: this.europeResult,
              nation: this.nation,
              //facebooks: facebook,
              //instagram: instagram,
              //snapchat: snapchat,
              first: 'true',
              status: 'active',
              photoURL: this.inputImageUser.nativeElement.value,
            });

            console.log(this.displayName);
            console.log(this.lastName);
            console.log(this.address);
            console.log(this.cardnumber);
            console.log(this.europeResult);
            console.log(this.nation);
            console.log(this.phone);

            user
              .updateProfile({
                displayName: this.displayName,
                photoURL: this.inputImageUser.nativeElement.value,
              })
              .then(() => {
                this.router.navigate(['admin/list-books']);
              })
              .catch((error) => console.log('error', error));
          }
        });
      })
      .catch((err) => console.log('err', err.message));
  }
  onLoginGoogle(): void {
    this.authService
      .loginGoogleUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log('err', err.message));
  }
  onLoginFacebook(): void {
    this.authService
      .loginFacebookUser()
      .then((res) => {
        this.onLoginRedirect();
      })
      .catch((err) => console.log('err', err.message));
  }

  onLoginRedirect(): void {
    this.router.navigate(['restaurants']);
  }
}
