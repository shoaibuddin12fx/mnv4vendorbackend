import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DataApiService } from '../../services/data-api.service';
import { BookInterface } from '../../models/book';
import { NgForm } from '@angular/forms';

import {FirebaseService} from '../../services/firebase.service';

import { RestaurantInterface } from '../../models/restaurant'; 
import { CategoryInterface } from '../../models/category'; 
import { MonthReportInterface } from '../../models/monthreport'; 
import { OrderInterface } from '../../models/order';



import { ActivatedRoute, Params } from '@angular/router';

import {Router} from '@angular/router';


import { AuthService } from '../../services/auth.service';

import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-year-report',
  templateUrl: './year-report.component.html',
  styleUrls: ['./year-report.component.css']
})
export class YearReportComponent implements OnInit {
	
	public stats: any = {
    today: 0,
    yesterday: 0,
    thisMonth: 0,
    lastMonth: 0,
    thisYear: 0,
    lastYear: 0
  };
  
  orders: any;
  
  reportToday: any;
  todayTotal: any;
  reportMonth: any;
  monthTotal: any;
  reportYear: any;
  yearTotal: any;
  
  todayFee: any;
  monthFee: any;
  yearFee: any;
  
  todays: any;
  lastYear: any;
  lastMonth: any;
  yesterday: any;
  thisYear: any;
  thisMonth: any;
  thisDay: any;
  
  orderToday: any;
  results: any;
  
  private MonthReportInterface: MonthReportInterface[];  
  private OrderInterface: OrderInterface[];  

  constructor(private firebaseService:FirebaseService) { }

  ngOnInit() {
	  
	       let today = new Date();
	this.lastYear = today.getFullYear() - 1;
	this.lastMonth = (today.getMonth() > 0) ? today.getMonth() : 12;
	this.yesterday = new Date(Date.now() - 86400000);
	this.thisYear = today.getFullYear();
	this.thisMonth = today.getMonth() + 1;
	this.thisDay = today.getDate();
	
	
	this.firebaseService.getYearOrder(this.thisYear).snapshotChanges().subscribe(orderToday =>{
		  console.log(orderToday);
		  
					  
					  this.results = [];
					  
					  orderToday.forEach(item => {
						  
									console.log(item);
									  
								
									let a = item.payload.toJSON(); 
									a['$key'] = item.key;
									
									console.log(a);
									
									this.results.push(a as OrderInterface);
					  
					  });
		  
		 
		  
	
		  
		  
	  });
	  
	  
  }

}
