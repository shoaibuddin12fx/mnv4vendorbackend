export interface ItemInterface {
	
  $key?:string;
  available?:string;
  category?:string;
  description?:string;
  image?:string;
  name?:string;
  price?:string;
  stock?:string;
  categories?:string;
  percent?:string;
  image_firebase_url?:string;
  lenght?:string;
}
