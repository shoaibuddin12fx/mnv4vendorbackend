export interface Roles {
  editor?: boolean;
  admin?: boolean;
  vendor?: boolean;
}

export interface UserInterface {
  id?: string;
  name?: string;
  email?: string;
  password?: string;
  photoUrl?: string;
  ven_name?: string;
  ven_key?: string;
  roles: Roles;
}
