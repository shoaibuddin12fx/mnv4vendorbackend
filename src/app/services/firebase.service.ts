import { Injectable } from '@angular/core';

import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject,
} from '@angular/fire/database'; // Firebase modules for Database, Data list and Single object

import * as firebase from 'firebase';

import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  listings: AngularFireList<any[]>;
  listing: AngularFireObject<any>;

  restaurants: AngularFireList<any[]>;
  restaurantUser: AngularFireList<any>;
  restaurant: AngularFireObject<any>;

  categories: AngularFireList<any[]>;
  category_details: AngularFireObject<any>;

  items: AngularFireList<any[]>;
  item_details: AngularFireObject<any>;

  orders: AngularFireList<any[]>;
  order_details: AngularFireObject<any>;

  allTableOrders: AngularFireList<any[]>;
  allTableOrdersDetails: AngularFireObject<any>;

  cities: AngularFireList<any[]>;
  cityDetail: AngularFireObject<any>;

  districts: AngularFireList<any[]>;
  districtDetail: AngularFireObject<any>;

  streets: AngularFireList<any[]>;
  streetDetail: AngularFireObject<any>;

  buildings: AngularFireList<any[]>;
  buildingDetail: AngularFireObject<any>;

  extraItemList: AngularFireList<any[]>;
  extraItemDetail: AngularFireObject<any>;

  chooseCategories: AngularFireList<any[]>;
  category_choose_details: AngularFireObject<any>;

  itemChoose: AngularFireList<any[]>;
  item_choose_details: AngularFireObject<any>;

  extraChooseItemList: AngularFireList<any[]>;
  extraChooseItemDetail: AngularFireObject<any>;

  users: AngularFireList<any[]>;
  user_details: AngularFireObject<any>;

  folder: any;
  itemFolder: any;
  restaurantFolder: any;
  categoryFolder: any;
  orderList: any;
  itemExtraList: any;
  itemChooseExtraList: any;
  userDetail: any;
  allTableOrdersList: any;
  //allTableOrders: any;
  paypal: any;
  stripe: any;
  tax: any;

  productId: any;

  categorizedOrders: any;

  constructor(
    private af: AngularFireDatabase,
    private afsAuth: AngularFireAuth
  ) {
    this.listings = this.af.list('/listings') as AngularFireList<Listing[]>;
    this.restaurants = this.af.list('/restaurants') as AngularFireList<
      Listing[]
    >;
    this.categories = this.af.list('/category') as AngularFireList<Category[]>;
    this.items = this.af.list('/items') as AngularFireList<Item[]>;
    this.orders = this.af.list('/orders') as AngularFireList<Order[]>;
    this.orderList = firebase.database().ref('/orders');
    this.itemExtraList = firebase.database().ref('/items');
    this.userDetail = firebase.database().ref('/users');
    this.allTableOrders = this.af.list('/AllTableOrders') as AngularFireList<
      TableOrder[]
    >;
    this.allTableOrdersList = firebase.database().ref('/AllTableOrders');
    this.paypal = firebase.database().ref('/paypal');
    this.stripe = firebase.database().ref('/stripe');
    this.cities = this.af.list('/city') as AngularFireList<City[]>;
    this.districts = this.af.list('/districts') as AngularFireList<District[]>;
    this.streets = this.af.list('/streets') as AngularFireList<Street[]>;
    this.buildings = this.af.list('/apartments') as AngularFireList<Building[]>;

    this.chooseCategories = this.af.list('/categoryChoose') as AngularFireList<
      Category[]
    >;
    this.itemChoose = this.af.list('/itemChoose') as AngularFireList<Item[]>;
    this.itemChooseExtraList = firebase.database().ref('/itemChoose');

    this.categorizedOrders = firebase.database().ref('/categorizedOrders');

    this.tax = firebase.database().ref('/tax');

    this.folder = 'listingimages';
    this.itemFolder = 'itemimages';
    this.restaurantFolder = 'restaurantimages';
    this.categoryFolder = 'categoryimages';
  }

  removeCategorizedOrders(order_id) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    this.categorizedOrders.child(uid).child(order_id).remove();

    //  firebase.database().ref('categorizedOrders/').child(uid).child(order_id).set({
    //					info: "deleted"
    //});
  }

  newRestaurantOrder: AngularFireObject<any>;
  getNewOrderDetails(id) {
    this.newRestaurantOrder = this.af.object(
      '/orders/' + id
    ) as AngularFireObject<Order>;

    return this.newRestaurantOrder;
  }

  yearOrder: AngularFireList<any>;
  getYearOrder(year) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    this.yearOrder = this.af.list('/orderCompleted/' + uid, (ref) =>
      ref.orderByChild('year').equalTo(year)
    ) as AngularFireList<Order>;

    return this.yearOrder;
  }

  monthOrder: AngularFireList<any>;
  getMonthOrder(year, month) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    this.monthOrder = this.af.list('/orderCompleted/' + uid, (ref) =>
      ref.orderByChild('year').equalTo(year)
    ) as AngularFireList<Order>;

    return this.monthOrder;
  }

  todayOrder: AngularFireList<any>;
  getTodayOrder(year, month, day) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    this.todayOrder = this.af.list('/orderCompleted/' + uid, (ref) =>
      ref.orderByChild('year').equalTo(year)
    ) as AngularFireList<Order>;

    return this.todayOrder;
  }

  todayReport: AngularFireObject<any>;
  getTodayReport(year, month, day) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    this.todayReport = this.af.object(
      '/reportsDay/' + year + '/' + month + '/' + day + '/' + uid
    ) as AngularFireObject<Order>;

    return this.todayReport;
  }

  monthReport: AngularFireObject<any>;
  getMonthReport(year, month) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    this.monthReport = this.af.object(
      '/reportsMonth/' + year + '/' + month + '/' + uid
    ) as AngularFireObject<Order>;

    return this.monthReport;
  }

  yearsReport: AngularFireObject<any>;
  getYearReport(year) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    this.yearsReport = this.af.object(
      '/reports/' + year + '/' + uid
    ) as AngularFireObject<Order>;

    return this.yearsReport;
  }

  updateDayReport(order_details, total, status, fee) {
    console.log(order_details);
    console.log(total);
    console.log(status);

    var totals = parseFloat(total);

    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    var fees = parseFloat(fee);

    if (status.status == 'Delivered') {
      console.log('inside delivered');

      firebase
        .database()
        .ref(
          'reportsDay/' +
            order_details.year +
            '/' +
            order_details.month +
            '/' +
            order_details.day +
            '/' +
            uid +
            '/total'
        )
        .once('value')
        .then(function (snapshot) {
          console.log(snapshot.val());

          if (snapshot.exists()) {
            var totalYear = snapshot.val();

            var sumTotal = totalYear + totals;

            firebase
              .database()
              .ref(
                'reportsDay/' +
                  order_details.year +
                  '/' +
                  order_details.month +
                  '/' +
                  order_details.day +
                  '/' +
                  uid
              )
              .update({
                total: sumTotal,
              });
          } else {
            firebase
              .database()
              .ref(
                'reportsDay/' +
                  order_details.year +
                  '/' +
                  order_details.month +
                  '/' +
                  order_details.day +
                  '/' +
                  uid
              )
              .update({
                total: totals,
              });
          }
        });

      firebase
        .database()
        .ref(
          'reportsDay/' +
            order_details.year +
            '/' +
            order_details.month +
            '/' +
            order_details.day +
            '/' +
            uid +
            '/fee'
        )
        .once('value')
        .then(function (snapshot) {
          console.log(snapshot.val());

          if (snapshot.exists()) {
            var totalYearFee = snapshot.val();

            var sumFee = totalYearFee + fees;

            firebase
              .database()
              .ref(
                'reportsDay/' +
                  order_details.year +
                  '/' +
                  order_details.month +
                  '/' +
                  order_details.day +
                  '/' +
                  uid
              )
              .update({
                fee: sumFee,
              });
          } else {
            firebase
              .database()
              .ref(
                'reportsDay/' +
                  order_details.year +
                  '/' +
                  order_details.month +
                  '/' +
                  order_details.day +
                  '/' +
                  uid
              )
              .update({
                fee: fees,
              });
          }
        });
    }
  }

  updateMonthReport(order_details, total, status, fee) {
    console.log(order_details);
    console.log(total);
    console.log(status);

    var totals = parseFloat(total);

    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    var fees = parseFloat(fee);

    if (status.status == 'Delivered') {
      console.log('inside delivered');

      firebase
        .database()
        .ref(
          'reportsMonth/' +
            order_details.year +
            '/' +
            order_details.month +
            '/' +
            uid +
            '/total'
        )
        .once('value')
        .then(function (snapshot) {
          console.log(snapshot.val());

          if (snapshot.exists()) {
            var totalYear = snapshot.val();

            var sumTotal = totalYear + totals;

            firebase
              .database()
              .ref(
                'reportsMonth/' +
                  order_details.year +
                  '/' +
                  order_details.month +
                  '/' +
                  uid
              )
              .update({
                total: sumTotal,
              });
          } else {
            firebase
              .database()
              .ref(
                'reportsMonth/' +
                  order_details.year +
                  '/' +
                  order_details.month +
                  '/' +
                  uid
              )
              .update({
                total: totals,
              });
          }
        });

      firebase
        .database()
        .ref(
          'reportsMonth/' +
            order_details.year +
            '/' +
            order_details.month +
            '/' +
            uid +
            '/fee'
        )
        .once('value')
        .then(function (snapshot) {
          console.log(snapshot.val());

          if (snapshot.exists()) {
            var totalYearFee = snapshot.val();

            var sumFee = totalYearFee + fees;

            firebase
              .database()
              .ref(
                'reportsMonth/' +
                  order_details.year +
                  '/' +
                  order_details.month +
                  '/' +
                  uid
              )
              .update({
                fee: sumFee,
              });
          } else {
            firebase
              .database()
              .ref(
                'reportsMonth/' +
                  order_details.year +
                  '/' +
                  order_details.month +
                  '/' +
                  uid
              )
              .update({
                fee: fees,
              });
          }
        });
    }
  }

  updateYearReport(order_details, total, status, fee) {
    console.log(order_details);
    console.log(total);
    console.log(status);

    var totals = parseFloat(total);

    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    var fees = parseFloat(fee);

    if (status.status == 'Delivered') {
      console.log('inside delivered');

      firebase
        .database()
        .ref('reports/' + order_details.year + '/' + uid + '/total')
        .once('value')
        .then(function (snapshot) {
          console.log(snapshot.val());

          if (snapshot.exists()) {
            var totalYear = snapshot.val();

            var sumTotal = totalYear + totals;

            firebase
              .database()
              .ref('reports/' + order_details.year + '/' + uid)
              .update({
                total: sumTotal,
              });
          } else {
            firebase
              .database()
              .ref('reports/' + order_details.year + '/' + uid)
              .update({
                total: totals,
              });
          }
        });

      firebase
        .database()
        .ref('reports/' + order_details.year + '/' + uid + '/fee')
        .once('value')
        .then(function (snapshot) {
          console.log(snapshot.val());

          if (snapshot.exists()) {
            var totalYearFee = snapshot.val();

            var sumFee = totalYearFee + fees;

            firebase
              .database()
              .ref('reports/' + order_details.year + '/' + uid)
              .update({
                fee: sumFee,
              });
          } else {
            firebase
              .database()
              .ref('reports/' + order_details.year + '/' + uid)
              .update({
                fee: fees,
              });
          }
        });
    }
  }

  restaurantOwnerOrderx: AngularFireObject<any>;
  updateOwnerOrderStatus(id, order_details) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    console.log(order_details);
    this.restaurantOwnerOrderx = this.af.object(
      '/orderCompleted/' + uid + '/' + id
    ) as AngularFireObject<Order>;

    this.restaurantOwnerOrderx.update(order_details);
  }

  updateOrderStatus(id, order_details) {
    console.log(id);

    console.log(order_details.status);

    firebase.database().ref('orders/').child(id).update({
      status: order_details.status,
      fee: order_details.fee,
    });
  }

  getTaxPercentInfo() {
    return firebase.database().ref('/tax').child('percent');

    //this.restaurantOrderInfos = this.af.object('/orderCompleted/' + uid + '/' + order_id ) as AngularFireObject<Order>;

    //return this.restaurantOrderInfos;
  }

  getRestaurantCompletedOrderStatus2(order_id) {
    console.log(order_id);

    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    return firebase.database().ref('/orderCompleted' + uid + '/' + order_id);

    //this.restaurantOrderInfos = this.af.object('/orderCompleted/' + uid + '/' + order_id ) as AngularFireObject<Order>;

    //return this.restaurantOrderInfos;
  }

  taxPercent: AngularFireObject<any>;
  getTaxPercent() {
    this.taxPercent = this.af.object('/tax') as AngularFireObject<Order>;
    return this.taxPercent;
  }

  restaurantOwnerOrders: AngularFireObject<any>;
  createOwnerOrderStatus(id, order_details) {
    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    console.log(order_details);

    this.restaurantOwnerOrders = this.af.object(
      '/orderCompleted/' + uid + '/' + id
    ) as AngularFireObject<Order>;

    this.restaurantOwnerOrders.update(order_details);
  }

  restaurantOrderInfos: AngularFireObject<any>;
  getRestaurantCompletedOrderStatus(order_id) {
    console.log(order_id);

    let uid = this.afsAuth.auth.currentUser.uid;

    console.log(uid);

    this.restaurantOrderInfos = this.af.object(
      '/orderCompleted/' + uid + '/' + order_id
    ) as AngularFireObject<Order>;

    return this.restaurantOrderInfos;
  }

  restaurantOrder: AngularFireList<any>;
  getRestaurantOrders(ven_key) {
    // let uid = this.afsAuth.auth.currentUser.uid;

    // console.log(uid);

    // this.restaurantOrder = this.af.list(
    //   '/categorizedOrders/' + uid
    // ) as AngularFireList<Order>;

    // return this.restaurantOrder;
        this.restaurantOrder = this.af.list("/orders/", (ref) =>
          ref.orderByChild("vendors")
        ) as AngularFireList<Order>;
        return this.restaurantOrder;
  }

  foodsUser: AngularFireList<any>;
  getFoodUser(ven_key) {
    this.foodsUser = this.af.list('/items/', (ref) =>
      ref.orderByChild('vendor').equalTo(ven_key)
    ) as AngularFireList<Item>;
    return this.foodsUser;
  }

  categoriesUser: AngularFireList<any>;
  getCategoryUser(res_id) {

    this.categoriesUser = this.af.list("/category/", (ref) =>
      ref.orderByChild("res_id").equalTo(res_id)
    ) as AngularFireList<Category>;

    return this.categoriesUser;
  }

  getRestaurantUser($key) {
    this.restaurantUser = this.af.list('/restaurants/', (ref) =>
      ref.orderByChild('ven_name').equalTo($key)
    ) as AngularFireList<Restaurant>;

    return this.restaurantUser;
  }
  getUserDetail(uid) {
    this.user_details = this.af.object('/users/' + uid) as AngularFireObject<
      UserInterface
    >;
    return this.user_details;
  }

  updateChooseExtraItem(id, extraItem) {
    firebase
      .database()
      .ref('/items')
      .child(this.productId)
      .child('extraOptions')
      .child(id)
      .update({
        name: extraItem.name,
        selected: 'false',
        value: extraItem.value,
      });
  }

  getItemEditExtraDetail(id) {
    console.log(this.productId);

    this.extraItemDetail = this.af.object(
      '/items/' + this.productId + '/extraOptions/' + id
    ) as AngularFireObject<Extra>;
    return this.extraItemDetail;
  }

  getItemExtraDetail(id) {
    this.extraItemList = this.af.list(
      '/items/' + id + '/extraOptions/'
    ) as AngularFireList<Extra[]>;
    return this.extraItemList;
  }

  setProductId(id) {
    this.productId = id;
  }

  onExtraItemDelete(item_id, extra_id) {
    //private items = this.af.database.list('listings');
    //items.remove(category);
    this.itemExtraList
      .child(item_id)
      .child('extraOptions')
      .child(extra_id)
      .remove();
  }

  addExtraItem(id, extraItem) {
    console.log(id);
    console.log(extraItem);

    firebase.database().ref('/items').child(id).child('extraOptions').push({
      name: extraItem.name,
      selected: 'false',
      value: extraItem.price,
    });
  }

  addStripeConfiguration(stripe) {
    console.log(stripe);

    this.stripe.set({
      publishable: stripe.publishable,
      secret: stripe.secret,
    });
  }

  addPaypalConfiguration(paypal) {
    console.log(paypal);

    this.paypal.set({
      sandbox: paypal.sandbox,
      production: paypal.production,
    });
  }

  updateBuilding(id, building) {
    return this.buildings.update(id, building);
  }

  getBuildingDetails(id) {
    this.buildingDetail = this.af.object(
      '/apartments/' + id
    ) as AngularFireObject<Building>;
    return this.buildingDetail;
  }

  updateStreet(id, street) {
    return this.streets.update(id, street);
  }

  getStreetDetails(id) {
    this.streetDetail = this.af.object('/streets/' + id) as AngularFireObject<
      Street
    >;
    return this.streetDetail;
  }

  updateDistrict(id, district) {
    return this.districts.update(id, district);
  }

  getDistrictDetails(id) {
    this.districtDetail = this.af.object(
      '/districts/' + id
    ) as AngularFireObject<District>;
    return this.districtDetail;
  }

  updateCity(id, city) {
    return this.cities.update(id, city);
  }

  getCityDetails(id) {
    this.cityDetail = this.af.object('/city/' + id) as AngularFireObject<City>;
    return this.cityDetail;
  }

  addNewBuilding(buildingName) {
    return this.buildings.push(buildingName);
  }

  addNewStreet(streetName) {
    return this.streets.push(streetName);
  }

  addNewDistrict(districtName) {
    return this.districts.push(districtName);
  }

  addNewCity(cityName) {
    return this.cities.push(cityName);
  }

  deleteBuilding(buildingKey) {
    return this.buildings.remove(buildingKey);
  }

  getBuildings() {
    return this.buildings;
  }

  deleteStreet(streetKey) {
    return this.streets.remove(streetKey);
  }

  getStreets() {
    return this.streets;
  }

  getDistricts() {
    return this.districts;
  }

  deleteDistrict(districtKey) {
    return this.districts.remove(districtKey);
  }

  getCities() {
    return this.cities;
  }

  deleteCity(cityKey) {
    return this.cities.remove(cityKey);
  }

  updateRestaurantOrderStatus(id, order_details) {
    console.log(id);
    console.log(order_details);
    firebase.database().ref('/orders').child(id).update({
      status: order_details.status,
    });
  }

  getOrderDetail(id) {
    return this.orderList.child(id);
  }

  getOrders() {
    return this.orders;
  }

  addItem(item) {
    let storageRefItem = firebase.storage().ref();
    for (let selectedItemFile of [
      (<HTMLInputElement>document.getElementById('image')).files[0],
    ]) {
      //let path = '/${this.folder}/${selectedFile.name}';
      let pathItem = `/${this.itemFolder}/${selectedItemFile.name}`;
      let iRefItem = storageRefItem.child(pathItem);
      iRefItem.put(selectedItemFile).then((snapshot) => {
        item.image = pathItem;

        const uid = this.afsAuth.auth.currentUser.uid;

        console.log(uid);

        item.user_id = uid;

        let storageRef = firebase.storage().ref();
        let spaceRef = storageRef.child(item.image);

        console.log(item.image);
        storageRef
          .child(item.image)
          .getDownloadURL()
          .then((url) => {
            // Set image url
            console.log(url);

            item.image_firebase_url = url;

            return this.items.push(item);
          })
          .catch((error) => {
            console.log(error);
          });

        //return this.items.push(item);
      });
    }
  }

  updateItemWithImage(id, item) {
    return this.items.update(id, item);
  }

  updateItem(id, item) {
    return this.items.update(id, item);
  }

  deleteItem(id) {
    return this.items.remove(id);
  }

  getItemDetails(id) {
    this.item_details = this.af.object('/items/' + id) as AngularFireObject<
      Item
    >;
    return this.item_details;
  }

  getItems() {
    return this.items;
  }

  deleteCategory(id) {
    return this.categories.remove(id);
  }

  updateCategoryWithImage(id, category) {
    return this.categories.update(id, category);
  }

  updateCategory(id, category) {
    return this.categories.update(id, category);
  }

  getCategoryDetails(cat_id) {
    this.category_details = this.af.object(
      '/category/' + cat_id
    ) as AngularFireObject<Category>;
    return this.category_details;
  }

  addCategory(category) {
    let storageRefItem = firebase.storage().ref();
    for (let selectedItemFile of [
      (<HTMLInputElement>document.getElementById('image')).files[0],
    ]) {
      let pathItem = `/${this.categoryFolder}/${selectedItemFile.name}`;
      let iRefItem = storageRefItem.child(pathItem);
      iRefItem.put(selectedItemFile).then((snapshot) => {
        category.image = pathItem;

        const uid = this.afsAuth.auth.currentUser.uid;

        console.log(uid);

        category.user_id = uid;

        let storageRef = firebase.storage().ref();
        let spaceRef = storageRef.child(category.image);

        console.log(category.image);
        storageRef
          .child(category.image)
          .getDownloadURL()
          .then((url) => {
            console.log(url);

            category.firebase_url = url;

            return this.categories.push(category);
          })
          .catch((error) => {
            console.log(error);
          });
      });
    }
  }

  getCategories() {
    this.categories = this.af.list('/category') as AngularFireList<Category[]>;
    return this.categories;
  }

  updateRestaurant(id, restaurant) {
    return this.restaurants.update(id, restaurant);
  }

  updateRestaurantWithImage(id, restaurant) {
    return this.restaurants.update(id, restaurant);
  }

  getRestaurantDetails(id) {
    this.restaurant = this.af.object('/restaurants/' + id) as AngularFireObject<
      Restaurant
    >;
    return this.restaurant;
  }

  restaurantCategory: AngularFireList<any>;
  getRestaurantCategories(id) {
    //const uid = this.authService.getUserId();

    this.restaurantCategory = this.af.list('/category/', (ref) =>
      ref.orderByChild('res_name').equalTo(id)
    ) as AngularFireList<Category>;
    // .subscribe(data => {
    //  console.log(data);
    //});
    return this.restaurantCategory;
  }

  /**
    this.restaurantCategory = this.af.list('/category/',
            {
          query: {
            orderByChild: 'res_name',
            equalTo: id
          }
          }) as AngularFireList<Category>;
      return this.restaurantCategory;
    }
	
	*/

  deleteRestaurant(id) {
    return this.restaurants.remove(id);
  }

  getRestaurants() {
    //console.log(this.afsAuth);

    //let uid = this.afsAuth.authState.subscribe(user => user.uid);

    //console.log(uids);

    return this.restaurants;
  }

  addRestaurant(restaurant) {
    let storageRefItem = firebase.storage().ref();
    for (let selectedItemFile of [
      (<HTMLInputElement>document.getElementById('image')).files[0],
    ]) {
      //let path = '/${this.folder}/${selectedFile.name}';
      let pathItem = `/${this.restaurantFolder}/${selectedItemFile.name}`;
      let iRefItem = storageRefItem.child(pathItem);
      iRefItem.put(selectedItemFile).then((snapshot) => {
        restaurant.image = pathItem;

        const uid = this.afsAuth.auth.currentUser.uid;

        console.log(uid);

        restaurant.user_id = uid;

        let storageRef = firebase.storage().ref();
        let spaceRef = storageRef.child(restaurant.image);

        console.log(restaurant.image);
        storageRef
          .child(restaurant.image)
          .getDownloadURL()
          .then((url) => {
            // Set image url
            console.log(url);

            restaurant.firebase_url = url;

            return this.restaurants.push(restaurant);
          })
          .catch((error) => {
            console.log(error);
          });
      });
    }
  }
}

interface Listing {
  $key?: string;
  title?: string;
  type?: string;
  image?: string;
  city?: string;
  owner?: string;
  bedrooms?: string;
  path?: any;
}

interface Restaurant {
  $key?: string;
  address?: string;
  description?: string;
  image?: string;
  info?: string;
  lat?: string;
  long?: string;
  mark?: string;
  option?: string;
  outlet?: string;
  phonenumber?: string;
  title?: string;
  firebase_url?: string;
  user_id?: string;
}

interface Category {
  $key?: string;
  cat_id?: string;
  cat_name?: string;
  res_name?: string;
  image?: string;
  firebase_url?: string;
}

interface City {
  $key?: string;
  name?: string;
}

interface District {
  $key?: string;
  name?: string;
}

interface Street {
  $key?: string;
  name?: string;
}

interface Building {
  $key?: string;
  name?: string;
}

interface Item {
  $key?: string;
  available?: string;
  category?: string;
  description?: string;
  image?: string;
  name?: string;
  price?: string;
  stock?: string;
  categories?: string;
  percent?: string;
  image_firebase_url?: string;
  lenght?: string;
}

interface Extra {
  $key?: string;
  name: string;
  selected: string;
  value: string;
}

interface Order {
  $key?: string;
  address_id?: string;
  created?: string;
  item_qty?: string;
  order_date_time?: string;
  payment_id?: string;
  product_firebase?: string;
  product_id?: string;
  product_image?: string;
  product_price?: string;
  product_total_price?: string;
  restaurant_id?: string;
  restaurant_name?: string;
  status?: string;
  user_id?: string;
  user_name?: string;
  restaurant_owner_id?: string;
  checked?: string;
}

interface TableOrder {
  $key?: string;
  restaurant_id?: string;
  restaurant_address?: string;
  restaurant_description?: string;
  restaurant_backgroundImage?: string;
  restaurant_firebase_url?: string;
  restaurant_icon?: string;
  restaurant_iconText?: string;
  restaurant_images?: string;
  restaurant_info?: string;
  restaurant_lat?: string;
  restaurant_long?: string;
  restaurant_mark?: string;
  restaurant_market?: string;
  restaurant_option?: string;
  restaurant_outlet?: string;
  restaurant_phonenumber?: string;
  restaurant_show?: string;
  restaurant_subtitle?: string;
  restaurant_title?: string;
  date?: string;
  person?: string;
  time?: string;
  userId?: string;
  status?: string;
  timeStamp?: string;
  reverseOrder?: string;
}
