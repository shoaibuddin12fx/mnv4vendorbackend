// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    // apiKey: "AIzaSyC7OssIThUAOUo-QcjejeQ1-ytjw-QOElA",
    // authDomain: "multi4ionic.firebaseapp.com",
    // databaseURL: "https://multi4ionic.firebaseio.com",
    // projectId: "multi4ionic",
    // storageBucket: "multi4ionic.appspot.com",
    // messagingSenderId: "719400134543"
    apiKey: "AIzaSyDoKEwE9HuIrUvELx7Kz0HocXSbeo6G-Cc",
    authDomain: "multivend-kreate.firebaseapp.com",
    databaseURL: "https://multivend-kreate.firebaseio.com",
    projectId: "multivend-kreate",
    storageBucket: "multivend-kreate.appspot.com",
    messagingSenderId: "1031340289743",
    appId: "1:1031340289743:web:3588188762db30277291ba",
    measurementId: "G-G6GE9D8XEX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
